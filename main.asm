; Rumus Menghitung waktu tempuh : jarak tempuh/kecepatan | menghitung jarak tempuh : kecepatan x waktu tempuh.

org 100h

jmp start       
pesan:     db      'Selamat Datang Di Program Penghitung Waktu Tempuh Dan Jarak Tempuh... ',0dh,0ah, '$'
pesan1:    db      0dh,0ah,"1-Menghitung Waktu Tempuh. ",0dh,0ah,"2-Menghitung Jarak Tempuh. ",0dh,0ah, '$'
pesan2:    db      0dh,0ah,"Masukkan Kecepatan (Km/Jam) : $"
pesan3:    db      0dh,0ah,"Masukkan Waktu Tempuh (Jam) : $"
pesan4:    db      0dh,0ah,"Masukkan Jarak Tempuh (Km) : $"
pesan5:    db      0dh,0ah,"Maaf, Program Tidak Mengenali Input.. $" 
pesan6:    db      0dh,0ah,"Waktu Tempuh (Jam) : $"
pesan7:    db      0dh,0ah,"Jarak Tempuh (Km) : $" 
pesan8:    db      0dh,0ah ,'Terimakasih Telah menggunakan Program ini... ', 0Dh,0Ah, '$'

start:  mov ah,9
        mov dx, offset pesan 
        int 21h
        mov dx, offset pesan1
        int 21h
        mov ah,0                       
        int 16h
        cmp al,31h
        je waktu
        cmp al,32h
        je jarak
        mov ah,09h
        mov dx, offset pesan5
        int 21h
        mov ah,0
        int 16h
        jmp start 
    
            
waktu:      mov ah,09h
            mov dx, offset pesan4
            int 21h
            mov cx,0
            call InputNo
            push dx
            mov ah,9
            mov dx, offset pesan2
            int 21h 
            mov cx,0
            call InputNo
            pop bx
            mov ax,bx
            mov cx,dx
            mov dx,0
            mov bx,0
            div cx
            mov bx,dx
            mov dx,ax
            push bx 
            push dx 
            mov ah,9
            mov dx, offset pesan6
            int 21h
            mov cx,100
            pop dx
            call View
            pop bx
            cmp bx,0
            je exit 
            jmp exit
                      
                      
jarak:      mov ah,09h
            mov dx, offset pesan2
            int 21h
            mov cx,0
            call InputNo
            push dx
            mov ah,9
            mov dx, offset pesan3
            int 21h 
            mov cx,0
            call InputNo
            pop bx
            mov ax,dx
            mul bx 
            mov dx,ax
            push dx 
            mov ah,9
            mov dx, offset pesan7
            int 21h
            mov cx,100
            pop dx
            call View
            je exit 
            jmp exit
                    
            
InputNo:    mov ah,0
            int 16h    
            mov dx,0  
            mov bx,1 
            cmp al,0dh
            je FormNo
            sub ax,30h
            call ViewNo
            mov ah,0 
            push ax  
            inc cx   
            jmp InputNo        
   

FormNo:     pop ax  
            push dx      
            mul bx
            pop dx
            add dx,ax
            mov ax,bx       
            mov bx,10
            push dx
            mul bx
            pop dx
            mov bx,ax
            dec cx
            cmp cx,0
            jne FormNo
            ret   


       
       
View:  mov ax,dx
       mov dx,0
       div cx 
       call ViewNo
       mov bx,dx 
       mov dx,0
       mov ax,cx 
       mov cx,10
       div cx
       mov dx,bx 
       mov cx,ax
       cmp ax,0
       jne View
       ret


ViewNo:    push ax
           push dx
           mov dx,ax
           add dl,30h
           mov ah,2
           int 21h
           pop dx  
           pop ax
           ret
      
   
exit:   mov dx, offset pesan8
        mov ah, 09h
        int 21h  


        mov ah, 0
        int 16h

        ret  